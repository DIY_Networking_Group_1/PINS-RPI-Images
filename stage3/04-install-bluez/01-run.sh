#!/bin/bash -e

# Source: https://learn.adafruit.com/install-bluez-on-the-raspberry-pi/installation

# Install:
on_chroot << EOF
cd /home/pinner/
wget http://www.kernel.org/pub/linux/bluetooth/bluez-5.50.tar.xz
tar xvf bluez-5.50.tar.xz
cd bluez-5.50
./configure
make
make install
systemctl start bluetooth
systemctl enable bluetooth
cd ..
EOF

# Cleanup:
on_chroot << EOF
rm -f bluez-5.50.tar.xz
EOF
