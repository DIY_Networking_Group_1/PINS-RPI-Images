#!/bin/bash -e

# To compile source code with 8.1 e.g: g++-8.1.0 -std=c++17 -Wall -pedantic if_test.cpp -o if_test
# Source: https://solarianprogrammer.com/2017/12/08/raspberry-pi-raspbian-install-gcc-compile-cpp-17-programs/

# Update:
on_chroot << EOF
cd /home/pinner
git clone https://bitbucket.org/sol_prog/raspberry-pi-gcc-binary.git
cd raspberry-pi-gcc-binary
tar xf gcc-8.1.0.tar.bz2
mv gcc-8.1.0 /usr/local
cd ..
echo 'export PATH=/usr/local/gcc-8.1.0/bin:$PATH' >> .bashrc
source .bashrc
EOF

# Cleanup:
on_chroot << EOF
rm -rf raspberry-pi-gcc-binary
EOF
