#!/bin/bash -e

on_chroot << EOF
pip3 install paho-mqtt
pip3 install RPi.GPIO
cd /home/pinner/
git clone https://gitlab.com/DIY_Networking_Group_1/PINS-Sensor.git
cp PINS-Sensor/gen/sensors.py /opt/
sudo chown pinner /opt/sensors.py
EOF
