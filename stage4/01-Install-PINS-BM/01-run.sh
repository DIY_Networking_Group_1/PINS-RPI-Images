#!/bin/bash -e

# Install paho.mqtt.c:
on_chroot << EOF
git clone https://github.com/eclipse/paho.mqtt.c.git
cd paho.mqtt.c
make
make install
cd ..
EOF

# Install PINS-BM:
on_chroot << EOF
git clone https://gitlab.com/DIY_Networking_Group_1/PINS-BM.git
cd PINS-BM/
make init
make compilePi
make install
chown pinner /opt/pins
cd ..
EOF

# Cleanup:
on_chroot << EOF
rm -rf PINS-BM/
EOF

# Install PINS-BM service:
install -m 644 files/pinns.service "${ROOTFS_DIR}/etc/systemd/system/"
on_chroot << EOF
systemctl daemon-reload
systemctl enable pinns.service
EOF
